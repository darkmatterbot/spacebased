import data_handler as dh
from flask import Flask, jsonify, request
import json
from flask_cors import CORS


app = Flask("space_based_backend")
cors = CORS(app)


@app.route('/api/concerts', methods=['GET'])
def get_concerts():
    jsonified_data = jsonify(None)
    try:
        hz_client = dh.connect_hazelcast()
        concerts = dh.fetch_all_hazelcast(hz_client)
        # Mapping the concert data to the desired format
        concerts_mapped = [
            {"id": concert[0], "title": concert[1], "price": concert[2], "available": concert[3], "picture": concert[4]}
            for concert in concerts
        ]
        jsonified_data = jsonify(concerts_mapped)
        print(jsonified_data)
    finally:
        return jsonified_data


@app.route('/api/concerts/purchase', methods=['POST'])
def purchase_tickets():
    purchase_valid = False
    available_tickets = None
    available_old = None

    try:
        hz_client = dh.connect_hazelcast()
        data = request.get_data()
        purchase_valid, available_tickets, available_old = dh.validate_purchase(hz_client, data)
        hz_client.shutdown()
    except Exception as e:
        print(f"Error in purchase: {e}")
    finally:
        if purchase_valid:
            response_data = {
                'success': True,
                'availableTickets': available_tickets
            }
            return jsonify(response_data), 200
        elif available_tickets <= 0:
            response_data = {
                'success': False,
                'availableTickets': available_old
            }
            return jsonify(response_data), 410
        else:
            response_data = {
                'success': False,
            }
            return jsonify(response_data), 400


if __name__ == "__main__":
    app.run(debug=False, host='0.0.0.0')
