# Getting started with MySQL Docker Database

The `docker-compse.yml` file is used to start a container with a mysql database.
Simply start the container with 
```bash
docker compose up
```
This command will also launch an adminer container to have easy access to the tables.

## Important parameters
Below listed are some important parameters.

```yaml
services:
  mysql:
    environment:
      MYSQL_ROOT_PASSWORD: rootpassword
      MYSQL_DATABASE: mydatabase
      MYSQL_USER: myuser
      MYSQL_PASSWORD: mypassword
    volumes:
      - ./mysql_data:/var/lib/mysql

  adminer:
    ports:
      - "8090:8080"
```
With those lines we can manage the user and password. Also, we mount a volume to make the database persistent.
The default value is `./mysql_data` which will create a folder at the same path as the docker-compose file.
With the port mapping in the adminer panel we will be  able to access it at `localhost:8090`

## Init the Database
To be abele to init the database run following command:
```bash
pip install pymysql
```
Then execute the `database_init.py` to fill the database with its initial values.

```bash
pyton database_init.py
```
The output should then look like this:
```
Database contains:
	 (1, 'Rock Festival', 50.0, 200)
	 (2, 'Jazz Night', 40.0, 150)
	 (3, 'Pop Concert', 45.0, 180)
	 (4, 'Classical Music Evening', 60.0, 100)
```
