import data_handler as dh
import config
from database_init import init_db

def update_callback(values):
    print(f"Hazelcast update: {values}")
    new_value = values.value[3]
    id = values.key
    dh.update_db_available(id, new_value)


if __name__ == '__main__':
    print("database handler started")
    # Init BD
    init_db()
    # Init hazelcast
    dh.init_hazelcast()

    # Add Event listener to keep DB up to date
    hz_client = dh.connect_hazelcast()
    map = hz_client.get_map(config.HAZEL_MAP_NAME).blocking()
    map.add_entry_listener(include_value=True, updated_func=update_callback)
    while True:
        a = 0  # Loop to keep hazelcast callback alive
