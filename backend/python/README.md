# Backend Overview
## Structure
**database** - This directory contains the dockerfile to start and manage the mysql database.

**hazelcast** - This directory contains the dockerfile to start and manage hazelcast.

**this dir** - Here well have the logic for front and backend

## Dependencies
In order to be able to run the code install all needed packages with:
````bash
pip install the requirements (req.txt with: 'pip install -r ./req.txt')
````

## How to run
First activate the python environment.
Then start the hazelcast and sql container (make sure you're in the right directory).

```bash
path/to/databse   ❯ docker compose up 
path/to/hazelcast ❯ docker compose up 
(http://localhost:8090/ for the Adminer (myuser:mypassword) and http://localhost:8080/ for Hazelcast)
```
After that we can initialize both containers by executing following:
````bash
../backend ❯   python database_init.py    # init and fills the database with values 
../backend ❯   python data_handler.py          # inits the map on hazelcast
````

Now we are ready to launch the backend server with:
```bash
../backend ❯ python data_routing.py
```
The script **data_routing.py** will start a flask server which wil be accessed by the frontend after starting it with `npm start`