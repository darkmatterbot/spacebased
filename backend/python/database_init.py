import pymysql
import data_handler as dh


def create_table(connection):
    sql1 = '''DROP TABLE IF EXISTS ticket_shop'''

    sql2 = '''
    CREATE TABLE ticket_shop(
        id int NOT NULL AUTO_INCREMENT,
        title VARCHAR(255) NOT NULL,
        price float NOT NULL,
        available int NOT NULL,
        picture VARCHAR(1000) NOT NULL,
        PRIMARY KEY (id)
    )
    '''
    with connection.cursor() as cursor:
        cursor.execute(sql1)
        cursor.execute(sql2)


def init_fill_table(connection):
    sql1 = "INSERT INTO ticket_shop(title, price, available, picture) VALUES ('Rock Festival', 50.0, 20000, 'http://i.epvpimg.com/aosLeab.jpg')"
    sql2 = "INSERT INTO ticket_shop(title, price, available, picture) VALUES ('Jazz Night', 40.0, 15000, 'http://i.epvpimg.com/b8eKdab.jpg')"
    sql3 = "INSERT INTO ticket_shop(title, price, available, picture) VALUES ('Pop Concert', 45.0, 18000, 'http://i.epvpimg.com/S9Bqfab.jpg')"
    sql4 = "INSERT INTO ticket_shop(title, price, available, picture) VALUES ('Classical Music Evening', 60.0, 10000, 'http://i.epvpimg.com/pmEBfab.jpg')"

    with connection.cursor() as cursor:
        cursor.execute(sql1)
        cursor.execute(sql2)
        cursor.execute(sql3)
        cursor.execute(sql4)
    connection.commit()


def fetch_data(connection):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM ticket_shop")
        result = cursor.fetchall()
    return result


def init_db():
    connection = dh.connect_db()

    create_table(connection)
    init_fill_table(connection)
    data = fetch_data(connection)

    print("Database contains:")
    for row in data:
        print(f"\t {row}")

    connection.close()


if __name__ == '__main__':
    init_db()