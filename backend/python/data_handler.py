import hazelcast
import mysql.connector
import config
import json


def connect_db():
    try:
        conf = {
            'host': config.DB_HOST,
            'port': config.DB_PORT,
            'user': config.DB_USER,
            'password': config.DB_PASS,
            'database': config.DB_DB,
        }
        db_connection = mysql.connector.connect(**conf)
        return db_connection
    except Exception as e:
        print(f"DB-Connect-Error: {e}")


def fetch_db_table(connection):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM ticket_shop")
        table = cursor.fetchall()
    return table


def update_db_available(id, new_value):
    sql = f"UPDATE ticket_shop SET available={new_value} WHERE id={id}"

    db_con = connect_db()
    with db_con.cursor() as cursor:
        cursor.execute(sql)
    db_con.commit()
    db_con.close()


def fetch_db_available(id):
    sql = f"SELECT available FROM ticket_shop WHERE id={id}"

    db_con = connect_db()
    with db_con.cursor() as cursor:
        cursor.execute(sql)
        result = cursor.fetchone()[0]
    db_con.close()
    return result


def connect_hazelcast():
    client = hazelcast.HazelcastClient(
        cluster_members=["haz:5701"] # Auskommentieren wenn man Frontent ohne alles testen will.
    )
    return client


def fill_hazelcast(client, data):
    map = client.get_map(config.HAZEL_MAP_NAME).blocking()
    map.clear()
    for d in data:
        map.put(d[0], d)


def update_hazelcast(client, key, value):
    map = client.get_map(config.HAZEL_MAP_NAME).blocking()
    aa = map.get(key)
    db_id, title, price, available, img = map.get(key)
    available_old = available
    available -= value
    if available >= 0:
        map.put(key, (db_id, title, price, available, img))
        return True, available, available_old
    else:
        return False, available, available_old


def fetch_all_hazelcast(hz_client):
    map = hz_client.get_map(config.HAZEL_MAP_NAME).blocking()
    return map.values()


def validate_purchase(hz_client, data):
    try:
        data = json.loads(data)
        concert_id = int(data['concertId'])
        ticket_amount = int(data['numberOfTickets'])
        valid, available, available_old = update_hazelcast(hz_client, concert_id, ticket_amount)
        return valid, available, available_old
    except ValueError:
        print(ValueError)
        return False, None, None


def init_hazelcast():
    db_client = connect_db()
    hz_client = connect_hazelcast()

    data = fetch_db_table(db_client)
    fill_hazelcast(hz_client, data)

    hz_client.shutdown()
    db_client.close()


if __name__ == "__main__":
    db_client = connect_db()
    hz_client = connect_hazelcast()

    data = fetch_db_table(db_client)
    init_hazelcast()
    update_hazelcast(hz_client, 1, 1)

    hz_client.shutdown()
    db_client.close()
