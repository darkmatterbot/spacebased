# Quarkus Backend



## Getting started

The docker image for quarkus backend has to be pulled from here:

https://hub.docker.com/repository/docker/darkmatterdev/spacebased/general

To pull this Image:
```bash
docker pull darkmatterdev/spacebased
```

Now the complete project can be started with this command:
```bash
docker compose up
```
or
```bash
docker compose up -d
```
followed by
```bash
docker compose logs -f
```
### NOTE
If you change anything, then you have to build the corresponding docker images again:
```bash
docker compose up --build
```
## Overview
| port       | application          | url                                     |
|------      |----------------------|---------------------                    |
| 80, 3000   | frontend             | http://localhost, http://localhost:3000 |
| 3001       | metrics UI           | http://localhost:3001                   |
| 5000       | Backend API UI       | http://localhost:5000/q/swagger-ui      |
| 5432       | Postgres DB Server   | localhost:5432                          |

### logins
  - POSTGRES_USER: spacebased
  - POSTGRES_PASSWORD: juliollijonathan



## If you want to develop
### Backend
```bash
quarkus dev
```

### Frontend
```bash
cd frontend && npm i && npm run dev
```


