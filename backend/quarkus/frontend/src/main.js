import { createApp } from 'vue'
import App from './App.vue'

import 'primevue/resources/primevue.min.css';
import 'primeicons/primeicons.css';
import 'primeflex/primeflex.css';

import 'primevue/resources/themes/md-dark-indigo/theme.css'

import PrimeVue from 'primevue/config';
import Panel from 'primevue/panel';
import Divider from 'primevue/divider';
import Card from 'primevue/card';
import Chart from 'primevue/chart';
import SelectButton from 'primevue/selectbutton';

const app = createApp(App)

app.component('SelectButton', SelectButton);
app.component('Panel', Panel);
app.component('Card', Card);
app.component('Divider', Divider);
app.component('Chart', Chart);
app.mount('#app')

app.use(PrimeVue);
