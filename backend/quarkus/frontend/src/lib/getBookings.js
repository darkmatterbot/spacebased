
import axios from 'axios';
import { API_URL } from './env.js';
const CONCERTS_URL = API_URL + "/concerts"
const BOOKINGS_BY_MINUTE = API_URL + '/bookings/bookingsByMinute?minutes=';
const DURATIONS_BY_MINUTE = API_URL + '/bookings/durationsByMinute?minutes=';

export const getBookings = async (minutes) => {
    return await get(BOOKINGS_BY_MINUTE + minutes)
}

export const getDurations = async (minutes) => {
    return await get(DURATIONS_BY_MINUTE + minutes)
}

export const getConcerts = async () => {
    return await get(CONCERTS_URL)
}

const get = async (URL) => {
    try {
        const res = await axios.get(URL);
        return res.data
    } catch (err) {
        console.error(err.code)
    }
}