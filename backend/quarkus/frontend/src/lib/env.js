const hostname = window.location.hostname

const PROD_API = 'https://' + hostname + '/api';
const DEV_API = 'http://' + hostname + ':5000/api';
export let API_URL = null;

// websocket
export const uniqueId = Date.now().toString(36) + Math.random().toString(36).substring(2);
const PROD_WS_URL = 'wss://' + hostname + '/api/bookings/live/' + uniqueId;
const DEV_WS_URL = 'ws://' + hostname + ':5000/api/bookings/live/' + uniqueId;
export let WS_URL = null;

if (hostname === 'localhost' || hostname === '127.0.0.1' || hostname === '0.0.0.0') {
    API_URL = DEV_API;
    WS_URL = DEV_WS_URL;
} else {
    API_URL = PROD_API;
    WS_URL = PROD_WS_URL;
}

console.log("API_URL: ", API_URL)
console.log("WS_URL: ", WS_URL)