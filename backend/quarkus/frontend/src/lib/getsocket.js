const initialReconnectDelay = 1000;
const maxReconnectDelay = 16000;
let currentReconnectDelay = initialReconnectDelay;

import { WS_URL ,uniqueId} from './env.js';

export const getSocketConnection = async () => {
    try {
        let socket = new WebSocket(WS_URL);
        socket.onopen = () => {
            console.log("connected ", WS_URL);
            currentReconnectDelay = initialReconnectDelay;
        };
        socket.onmessage = (m) => {
            if ('ping' === m.data) {
                socket.send('pong')
            }
            else if (uniqueId === m.data) {
                console.log("NEED TO UPDATE DATA")
                // loadLazyData();
            }
        };

        socket.onclose = (event) => {
            socket = null;
            setTimeout(() => {
                reconnectToWebsocket();
            }, currentReconnectDelay + Math.floor(Math.random() * 1500));
        };
    } catch (err) {
        console.log(err);
    }
};
const reconnectToWebsocket = () => {
    if (currentReconnectDelay < maxReconnectDelay) {
        currentReconnectDelay *= 2;
    }
    getSocketConnection();
};