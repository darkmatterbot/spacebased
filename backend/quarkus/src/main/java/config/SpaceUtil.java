package config;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

import de.rwu.sa.db.Bookings;
import jakarta.enterprise.context.RequestScoped;
import jakarta.transaction.Transactional;

@RequestScoped
public class SpaceUtil {

	@Transactional
	public static Bookings saveBooking(long key, String platform, long startTime, long endTime) {

		long duration = endTime - startTime;
        LocalDateTime endTimeDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(endTime), ZoneId.systemDefault());
        
		Bookings b = new Bookings();
		b.setDuration(duration);
		b.setKey(key);
		b.setPlatform(platform);
		b.setTime(endTimeDateTime);
		b.persist();

		return b;
	}
}
