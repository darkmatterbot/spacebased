package config;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import io.quarkus.logging.Log;
import io.quarkus.runtime.StartupEvent;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.event.Observes;
import lombok.Data;

@Data
@ApplicationScoped
public class SpaceConfig {

	@ConfigProperty(name = "HAZEL_SERVER_ADDRESS")
	private String HAZEL_SERVER_ADDRESS;

	@ConfigProperty(name = "HAZEL_MAP_NAME")
	private String HAZEL_MAP_NAME;

	@ConfigProperty(name = "quarkus.rest-client.stress-test.url")
	private String STRESS_TEST_URL;

	void init(@Observes StartupEvent ev) {
		Log.info(StringUtils.repeat("-", 50));
		Log.info("HAZEL_SERVER_ADDRESS: " + HAZEL_SERVER_ADDRESS);
		Log.info("HAZEL_MAPE_NAME: " + HAZEL_MAP_NAME);
		Log.info("STRESS_TEST_URL: " + STRESS_TEST_URL);
		Log.info(StringUtils.repeat("-", 50));
	}

}
