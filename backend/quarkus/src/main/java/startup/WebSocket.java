package startup;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import io.quarkus.logging.Log;
import io.quarkus.scheduler.Scheduled;
import jakarta.inject.Singleton;
import jakarta.websocket.OnClose;
import jakarta.websocket.OnError;
import jakarta.websocket.OnOpen;
import jakarta.websocket.Session;
import jakarta.websocket.server.PathParam;
import jakarta.websocket.server.ServerEndpoint;

@Singleton
@ServerEndpoint(value = "/api/bookings/live/{id}")
public class WebSocket {
	static Map<String, Session> sessions = new ConcurrentHashMap<>();

	@Scheduled(every = "60s")
	public void pingClients() {
		for (Map.Entry<String, Session> session : sessions.entrySet()) {
			sendMessage(session.getValue(), "ping");
		}
	}

	@Scheduled(every = "1s")
	public static void notifyClients() {
		if (!sessions.isEmpty()) {
//			Log.info("notifying clients " + sessions.keySet());
			for (Map.Entry<String, Session> session : sessions.entrySet()) {
				sendMessage(session.getValue(), session.getKey());
			}
		}
	}

	@OnOpen
	private void onOpen(Session session, @PathParam("id") String id) {
		sessions.put(id, session);
		sendMessage(session, "moin");
		Log.info("User " + id + " opened session");
	}

	private static void sendMessage(Session session, String msg) {
		session.getAsyncRemote().sendObject(msg, res -> {
			if (res.getException() != null) {
				Log.error("Unable to send message: " + res.getException());
			}
		});
	}

	@OnClose
	private void onClose(Session session, @PathParam("id") String id) {
		sessions.remove(id);
		Log.info("User " + id + " left");
	}

	@OnError
	private void onError(Session session, @PathParam("id") String id, Throwable throwable) {
		sessions.remove(id);
		Log.info("User " + id + " left on error: " + throwable);
	}
}
