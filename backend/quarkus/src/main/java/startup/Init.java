package startup;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Optional;

import config.SpaceUtil;
import de.rwu.sa.db.TestData;
import de.rwu.sa.db.TicketShop;
import hazel.HazelClient;
import io.quarkus.logging.Log;
import io.quarkus.runtime.Startup;
import io.quarkus.runtime.annotations.RegisterForReflection;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;

@Singleton
@RegisterForReflection
public class Init {

	@Inject
	HazelClient hzClient;

	@Inject
	EntityManager em;

	private static final String CREATE_F1 = getFile(
			"db/migration/V1__create_function_fetch_bookings_by_last_seconds.sql");
	private static final String CREATE_F2 = getFile(
			"db/migration/V1__create_function_fetch_durations_by_last_seconds.sql");

	@Startup
	public void init() {
		initConcerts(1500);
		init_db_functions();
		hzClient.initHazel();
	}

	@Transactional
	public boolean initConcerts(int available) {
		try {
			for (int i = 0; i < TestData.TEST_DATA.size(); i++) {
				TicketShop testDataEntry = TestData.TEST_DATA.get(i);
				Optional<TicketShop> existingEntry = TicketShop.find("title", testDataEntry.getTitle())
						.firstResultOptional();
				if (existingEntry.isEmpty()) {
					Log.info("EMPTY");
					testDataEntry.setAvailable(available);
					testDataEntry.persist();
				} else {
					existingEntry.get().setAvailable(available);
					existingEntry.get().persist();
				}
			}
			SpaceUtil.saveBooking(1, "Hazel Cast", System.currentTimeMillis(), System.currentTimeMillis());
			SpaceUtil.saveBooking(2, "Database", System.currentTimeMillis(), System.currentTimeMillis());

		} catch (Exception e) {
//			Log.error(e);
			e.printStackTrace();
			return false;
		}

		return true;
	}

	@Transactional
	public void init_db_functions() {
		em.createNativeQuery(CREATE_F1).executeUpdate();
		em.createNativeQuery(CREATE_F2).executeUpdate();
	}

	public static String getFile(String file) {
		InputStream indexIS = null;
		try {
			indexIS = Init.class.getClassLoader().getResourceAsStream(file);
			final InputStreamReader isr = new InputStreamReader(indexIS, StandardCharsets.UTF_8);
			final StringBuilder sb = new StringBuilder();
			int c;
			while ((c = isr.read()) != -1) {
				sb.append((char) c);
			}
			return sb.toString();
		} catch (Exception e) {
			Log.info(file + ": " + e.getLocalizedMessage());
		} finally {
			if (indexIS != null) {
				try {
					indexIS.close();
				} catch (IOException e) {
					Log.error(e);
				}
			}
		}
		return null;
	}
}
