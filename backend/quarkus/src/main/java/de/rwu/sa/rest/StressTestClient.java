package de.rwu.sa.rest;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import io.quarkus.runtime.annotations.RegisterForReflection;
import jakarta.json.JsonObject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.MediaType;

@Path("/api/concerts/purchase")
@RegisterRestClient(configKey = "stress-test")
@RegisterForReflection
public interface StressTestClient {

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	void purchase(JsonObject request);
}
