package de.rwu.sa.db;

import java.util.Arrays;
import java.util.List;

public class TestData {

	public static final List<TicketShop> TEST_DATA = Arrays.asList(
			new TicketShop("Rock Festival", 50, 1500, "http://i.epvpimg.com/aosLeab.jpg"),
			new TicketShop("Jazz Night", 40, 150, "http://i.epvpimg.com/b8eKdab.jpg"),
			new TicketShop("Pop Concert", 45, 180, "http://i.epvpimg.com/S9Bqfab.jpg"),
			new TicketShop("Classical Music Evening", 60, 100, "http://i.epvpimg.com/pmEBfab.jpg"));
}
