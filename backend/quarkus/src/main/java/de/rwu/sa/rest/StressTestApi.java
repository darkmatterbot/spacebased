package de.rwu.sa.rest;

import java.util.List;
import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.stream.IntStream;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.jboss.resteasy.reactive.RestQuery;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import hazel.HazelClient;
import io.quarkus.logging.Log;
import io.quarkus.runtime.annotations.RegisterForReflection;
import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Inject;
import jakarta.json.Json;
import jakarta.json.JsonObject;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Response;

@Path("/")
@RequestScoped
@RegisterForReflection
public class StressTestApi {

	@Inject
	ObjectMapper mapper;

	@Inject
	@RestClient
	StressTestClient stressTestClient;

	@Inject
	HazelClient hzClient;

	@POST
	@Path("/start")
	public Response startTest(@RestQuery @DefaultValue("10") Integer requestCount) {
		sendRequests(requestCount);
		ObjectNode res = mapper.createObjectNode();
		res.put("sending requests", requestCount);
		return Response.ok(res).build();
	}

	@SuppressWarnings("unchecked")
	public boolean sendRequests(int requestCount) {
		List<Long> keys = hzClient.getTicketShopMap().keySet().stream().toList();
		if (keys.isEmpty()) {
			throw new IllegalStateException("No keys available in the map");
		}

		// Generate a list of JSON bodies based on the keys
		List<JsonObject> jsonBodies = keys.stream()
				.map(key -> Json.createObjectBuilder().add("concertId", key).add("numberOfTickets", 1).build())
				.toList();

		Random random = new Random();

		IntStream.range(0, requestCount).mapToObj(i -> {
			return CompletableFuture
					.runAsync(() -> stressTestClient.purchase(jsonBodies.get(random.nextInt(jsonBodies.size()))))
					.exceptionally(ex -> {
						// Handle exception
						Log.error("Exception occurred: " + ex.getMessage());
						return null; // or handle the exception and return a default value or a fallback action
					});
		}).toArray(CompletableFuture[]::new);

		return true;
	}
}
