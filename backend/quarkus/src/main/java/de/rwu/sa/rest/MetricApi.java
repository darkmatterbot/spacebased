package de.rwu.sa.rest;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jboss.resteasy.reactive.RestQuery;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import de.rwu.sa.db.Bookings;
import de.rwu.sa.db.TicketShop;
import io.quarkus.runtime.annotations.RegisterForReflection;
import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import model.BookingCount;
import model.ConcertBookingSummary;
import model.Duration;
import model.Metrics;
import model.PlatformSummary;

@Path("/api/bookings")
@RequestScoped
@RegisterForReflection
public class MetricApi {

	@Inject
	ObjectMapper mapper;

	@Inject
	EntityManager em;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response concerts() {
		return Response.ok(Bookings.listAll()).build();
	}

	@GET
	@Path("/bookingsByMinute")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRecentBookingsByMinute(@RestQuery @DefaultValue("5") int minutes) {
		return Response.ok(fetchBookingCountsForLastNMinutes(minutes)).build();
	}

	@GET
	@Path("/durationsByMinute")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRecentDurationsByMinute(@RestQuery @DefaultValue("5") int minutes) {
		return Response.ok(getRecentDurations(minutes)).build();
	}

	@GET
	@Path("/metrics")
	public Response metrics(@RestQuery @DefaultValue("5") int minutes) {
		return Response.ok(getMetrics(minutes)).build();
	}

	@GET
	@Path("/reset")
	@Transactional
	@Produces(MediaType.APPLICATION_JSON)
	public Response addConcert() {
		long deleted = Bookings.deleteAll();
		ObjectNode res = mapper.createObjectNode();
		res.put("deleted", deleted);
		return Response.ok().entity(res).build();
	}

	@SuppressWarnings("unchecked")
	private List<ConcertBookingSummary> fetchBookingCountsForLastNMinutes(int minutes) {

		String sqlQuery = "SELECT * FROM fetch_bookings_by_last_seconds(" + minutes * 60 + ");";
		List<Object[]> results = em.createNativeQuery(sqlQuery).getResultList();

		Map<Long, ConcertBookingSummary> concertMap = new HashMap<>();

		for (Object[] result : results) {
			LocalDateTime ts = ((Timestamp) result[0]).toLocalDateTime(); // Convert Timestamp to LocalDateTime
			Long concertId = ((Number) result[1]).longValue();
			Long cnt = ((Number) result[2]).longValue();

			String title = "";
			 TicketShop t = TicketShop.find("id", concertId).firstResult();
			 if(t!=null) {
				 title = t.getTitle();
			 }
			if (!concertMap.containsKey(concertId)) {
				ConcertBookingSummary concertSummary = new ConcertBookingSummary(concertId, title, new ArrayList<>());
				concertMap.put(concertId, concertSummary);
			}
			ConcertBookingSummary concertSummary = concertMap.get(concertId);
			concertSummary.getData().add(new BookingCount(ts, cnt));
		}

		return new ArrayList<>(concertMap.values());
	}

	@SuppressWarnings("unchecked")
	public List<PlatformSummary> getRecentDurations(int minutes) {
		String sqlQuery = "SELECT * FROM fetch_durations_by_last_seconds(" + minutes * 60 + ")";
		List<Object[]> results = em.createNativeQuery(sqlQuery).getResultList();

		Map<String, PlatformSummary> platformMap = new HashMap<>();

		List<Metrics> metrics = getMetrics(minutes);

		for (Object[] result : results) {
			LocalDateTime ts = ((Timestamp) result[0]).toLocalDateTime();
			String platform = (String) result[1];
			int avgDuration = ((Number) result[2]).intValue();

			if (!platformMap.containsKey(platform)) {
				Metrics metric = null;
				for (Metrics c : metrics) {
					if (c.getPlatform().equals(platform)) {
						metric = c;
					}
				}
				if (metric == null) {
					metric = new Metrics(platform, 0, 0, 0, 0);
				}
				PlatformSummary platformSummary = new PlatformSummary(platform, metric, new ArrayList<>());
				platformMap.put(platform, platformSummary);
			}

			PlatformSummary platformSummary = platformMap.get(platform);
			platformSummary.getData().add(new Duration(ts, avgDuration));
		}

		return new ArrayList<>(platformMap.values());
	}

	@SuppressWarnings("unchecked")
	public List<Metrics> getMetrics(int minutes) {
		String sql = "SELECT platform, AVG(duration) AS average_, STDDEV(duration) as std, min(duration) as min_, max(duration) as max_ FROM Bookings GROUP BY platform;";
		List<Object[]> results = em.createNativeQuery(sql).getResultList();

		List<Metrics> metrics = new ArrayList<>();

		for (Object[] result : results) {
			try {
				String platform = (result[0]).toString(); // Convert Timestamp to LocalDateTime
				float avg = ((Number) result[1]).longValue();
				float std = ((Number) result[2]).longValue();
				int min = ((Number) result[3]).intValue();
				int max = ((Number) result[4]).intValue();
				metrics.add(new Metrics(platform, avg, std, min, max));
			} catch (Exception ignore) {
			}
		}
		return metrics;
	}

}
