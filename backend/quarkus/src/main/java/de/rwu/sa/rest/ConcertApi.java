package de.rwu.sa.rest;

import org.jboss.resteasy.reactive.RestQuery;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import de.rwu.sa.db.TicketShop;
import hazel.HazelClient;
import io.quarkus.runtime.Startup;
import io.quarkus.runtime.annotations.RegisterForReflection;
import io.vertx.core.json.JsonObject;
import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import startup.Init;

@Path("/api/concerts")
@RequestScoped
@RegisterForReflection
public class ConcertApi {

	@Inject
	ObjectMapper mapper;

	@Inject
	HazelClient hzClient;

	@Inject
	Init init;

	static Response ERROR_RESPONSE = null;
	static Response SUCCESS_RESPONSE = null;

	@Startup
	void init() {
		ObjectNode err = mapper.createObjectNode();
		err.put("success", false);

		ObjectNode success = mapper.createObjectNode();
		success.put("success", true);

		SUCCESS_RESPONSE = Response.ok(success).build();

		ERROR_RESPONSE = Response.status(400).entity(err).build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response concerts() {
		return Response.ok(TicketShop.list("ORDER BY title DESC")).build();
	}

	@GET
	@Path("/reset")
	@Transactional
	@Produces(MediaType.APPLICATION_JSON)
	public Response resetConcerts(@RestQuery @DefaultValue("1500") int available) {
		boolean success = hzClient.resetConcerts(available);
		if (success)
			return Response.status(200).entity(SUCCESS_RESPONSE).build();
		return Response.status(400).entity(ERROR_RESPONSE).build();
	}

	@POST
	@Path("/add")
	@Transactional
	@Produces(MediaType.APPLICATION_JSON)
	public Response addConcert(@RestQuery String title, @RestQuery float price, @RestQuery int available,
			@RestQuery String picture) {
		TicketShop ts = new TicketShop();
		ts.setAvailable(available);
		ts.setPicture(picture);
		ts.setTitle(title);
		ts.setPrice(price);
		ts.persist();

		hzClient.addConcert(ts.id, available);
		return Response.status(200).entity(ts).build();
	}

	@POST
	@Path("/purchase")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response purchase(JsonObject request) {
		long concertId = request.getLong("concertId").longValue();
		int numberOfTickets = request.getInteger("numberOfTickets");

		boolean purchaseValid = hzClient.tryPurchaseTicket(concertId, numberOfTickets);

		if (purchaseValid) {
			return Response.ok(SUCCESS_RESPONSE).build();
		}
		return Response.status(Response.Status.BAD_REQUEST).entity(ERROR_RESPONSE).build();
	}
}
