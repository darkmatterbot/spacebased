package hazel;

import com.hazelcast.core.EntryEvent;
import com.hazelcast.map.listener.EntryAddedListener;
import com.hazelcast.map.listener.EntryRemovedListener;
import com.hazelcast.map.listener.EntryUpdatedListener;

import config.SpaceUtil;
import de.rwu.sa.db.TicketShop;
import io.quarkus.arc.Arc;
import io.quarkus.arc.ManagedContext;
import io.quarkus.logging.Log;
import io.quarkus.narayana.jta.QuarkusTransaction;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class TicketShopListener implements EntryAddedListener<Long, Integer>, EntryRemovedListener<Long, Integer>,
		EntryUpdatedListener<Long, Integer> {

	@Override
	public void entryAdded(EntryEvent<Long, Integer> event) {
		Log.info("ADD: " + event);
	}

	@Override
	public void entryUpdated(EntryEvent<Long, Integer> event) {
//		Log.info("UPDATE: " + event);

		long key = event.getKey();
		int availableAfterPurchase = event.getValue();

		ManagedContext requestContext = Arc.container().requestContext();
		if (!requestContext.isActive()) {
			requestContext.activate();
		}

		try {
			long startTime = System.currentTimeMillis();
			QuarkusTransaction.begin();
			TicketShop.update("available = " + availableAfterPurchase + "  where id = ?1", key);
			QuarkusTransaction.commit();

			SpaceUtil.saveBooking(key, "Database", startTime, System.currentTimeMillis());
//			Log.info("BOOKING ID: " + b.id);
//			WebSocket.notifyClients();

		} catch (Exception e) {
			Log.error("Failed to update ticket shop", e);
		} finally {
			requestContext.terminate();
		}
	}

	@Override
	public void entryRemoved(EntryEvent<Long, Integer> event) {
		Log.info("REMOVED: " + event);
	}

}
