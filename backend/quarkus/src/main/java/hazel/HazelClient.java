package hazel;

import java.util.List;

import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.map.IMap;

import config.SpaceConfig;
import config.SpaceUtil;
import de.rwu.sa.db.TicketShop;
import io.quarkus.logging.Log;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import lombok.Getter;

@Getter
@ApplicationScoped
public class HazelClient {

	@Inject
	SpaceConfig config;

	private HazelcastInstance hzClient;

	@SuppressWarnings("rawtypes")
	@Getter
	private IMap ticketShopMap;

	@SuppressWarnings("unchecked")
	public void initHazel() {
		// Create client configuration --> specified HAZEL_SERVER_ADDRESS
		ClientConfig clientConfig = new ClientConfig();
		clientConfig.getNetworkConfig().addAddress(config.getHAZEL_SERVER_ADDRESS());

		// Start the Hazelcast Client and connect to an already running Hazelcast
		// Cluster
		hzClient = HazelcastClient.newHazelcastClient(clientConfig);

		// Get the Distributed Map from Cluster.
		ticketShopMap = hzClient.getMap(config.getHAZEL_MAP_NAME());

		// Create and add entry listener
		TicketShopListener entryListener = new TicketShopListener();
		ticketShopMap.addEntryListener(entryListener, true);

		ticketShopMap.clear();

		List<TicketShop> tl = TicketShop.listAll();
		for (TicketShop t : tl) {
			ticketShopMap.put(t.id, t.getAvailable());
		}
	}

	@SuppressWarnings("unchecked")
	public boolean tryPurchaseTicket(long concertId, int numberOfTickets) {
		if (hzClient == null || ticketShopMap == null) {
			initHazel();
		}

		boolean success = false;

		// lock the map
		ticketShopMap.lock(concertId);
		try {
			if (ticketShopMap.containsKey(concertId)) {
				Object mapValue = ticketShopMap.get(concertId);
				if (mapValue != null) {
					int oldNumberOfTickets = (int) mapValue;
					success = tryBooking(concertId, oldNumberOfTickets, numberOfTickets);
				} else {
					Log.error("Value for map-Key [" + concertId + "] is null");
				}
			} else {
				Log.error("Key [" + concertId + "] not found in map: " + config.getHAZEL_MAP_NAME());
			}

		} finally {
			ticketShopMap.unlock(concertId);
		}
		return success;
	}

	@SuppressWarnings("unchecked")
	private boolean tryBooking(long concertId, int oldNumberOfTickets, int numberOfTickets) {
		int availablNumberOfTicketsAfterBooking = oldNumberOfTickets - numberOfTickets;
		if (availablNumberOfTicketsAfterBooking >= 0) {
			long startTime = System.currentTimeMillis();
			ticketShopMap.put(concertId, availablNumberOfTicketsAfterBooking);
			SpaceUtil.saveBooking(concertId, "Hazel Cast", startTime, System.currentTimeMillis());
//			WebSocket.notifyClients();
			return true;
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	public boolean addConcert(long concertId, int availableTickets) {
		if (hzClient == null || ticketShopMap == null) {
			initHazel();
		}

		try {
			ticketShopMap.lock(concertId);
			ticketShopMap.put(concertId, availableTickets);
		} finally {
			ticketShopMap.unlock(concertId);
		}

		return true;
	}

	@SuppressWarnings("unchecked")
	public boolean resetConcerts(int availableTickets) {
		if (hzClient == null || ticketShopMap == null) {
			initHazel();
		}

		boolean success = true;

		List<Long> keys = ticketShopMap.keySet().stream().toList();

		for (Long concertId : keys) {
			try {
				// lock the map
				ticketShopMap.lock(concertId);
				if (!ticketShopMap.containsKey(concertId)) {
					Log.error("Key [" + concertId + "] not found in map: " + config.getHAZEL_MAP_NAME());
					success = false;
					break;
				}
				long startTime = System.currentTimeMillis();
				ticketShopMap.put(concertId, availableTickets);
				SpaceUtil.saveBooking(concertId, "Hazel Cast", startTime, System.currentTimeMillis());
//				WebSocket.notifyClients(b);
			} finally {
				ticketShopMap.unlock(concertId);
			}
		}

		return success;
	}

	public void shutdownHzClient() {
		hzClient.shutdown();
	}
}
