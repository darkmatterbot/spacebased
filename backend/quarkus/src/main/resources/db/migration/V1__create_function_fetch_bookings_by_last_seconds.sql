CREATE OR REPLACE FUNCTION fetch_bookings_by_last_seconds(seconds integer)
RETURNS TABLE (
    ts TIMESTAMP,
    concertId BIGINT,
    booking_count BIGINT
)
LANGUAGE SQL
AS $$
WITH second_series AS (
    SELECT generate_series(
        date_trunc('second', now()) - (seconds || ' seconds')::interval,
        date_trunc('second', now()),
        '1 second'
    ) AS ts
),
keys_ AS (
    SELECT DISTINCT key FROM Bookings
)
SELECT ss.ts,
       p.key,
       COALESCE(COUNT(b.id), 0) as booking_count
FROM second_series ss
CROSS JOIN keys_ p
LEFT JOIN Bookings b ON date_trunc('second', b.time) = ss.ts 
                     AND b.key = p.key
                     AND b.platform = 'Database'
GROUP BY ss.ts, p.key
ORDER BY ss.ts, p.key
$$;