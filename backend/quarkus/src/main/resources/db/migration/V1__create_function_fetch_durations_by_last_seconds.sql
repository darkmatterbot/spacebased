CREATE OR REPLACE FUNCTION fetch_durations_by_last_seconds(seconds integer)
RETURNS TABLE (
    ts TIMESTAMP,
    platform TEXT,
    avg_duration BIGINT,
    booking_count BIGINT
)
LANGUAGE SQL
AS $$
WITH second_series AS (
    SELECT generate_series(
        date_trunc('second', now()) - (seconds || ' seconds')::interval,
        date_trunc('second', now()),
        '1 second'
    ) AS ts
),
platforms AS (
    SELECT DISTINCT platform
    FROM Bookings
)
SELECT ss.ts,
       p.platform,
       COALESCE(bc.average_duration, 0) AS average_duration,
       COALESCE(bc.booking_count, 0) AS booking_count
FROM second_series ss
CROSS JOIN platforms p
LEFT JOIN (
    SELECT date_trunc('second', b.time) AS ts,
           b.platform,
           COUNT(b.id) AS booking_count,
           CASE WHEN COUNT(b.id) >= 1 THEN AVG(b.duration) ELSE 0 END AS average_duration
    FROM Bookings b
    WHERE b.time >= date_trunc('second', now()) - (seconds|| ' seconds')::interval
      AND b.time <= date_trunc('second', now())
    GROUP BY date_trunc('second', b.time), b.platform
) bc ON ss.ts = bc.ts AND p.platform = bc.platform
ORDER BY ss.ts, p.platform
$$;