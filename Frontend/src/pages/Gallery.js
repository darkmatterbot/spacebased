import React from 'react';
import { Container, Row, Col, Card } from 'react-bootstrap';

const concertImages = [
  {
    src: 'https://www.visitberlin.de/system/files/styles/visitberlin_bleed_header_visitberlin_tablet_portrait_2x/private/image/Treptow-Koepenick%20%289%29%20Wuhlheide_c_Tourismusverein_Berlin_Treptow_Koepenick_e.V_DL_PPT_0.jpg.webp?h=e5aec6c8&itok=IE8rUIt1',
    title: 'Konzert in Berlin',
    description: 'Ein atemberaubendes Live-Konzert in Berlin mit fantastischer Atmosphäre. Die Bühne leuchtete eindrucksvoll, und die Menge tobte vor Begeisterung. Leidenschaftliche Musiker und mitreißende Melodien machten die Nacht unvergesslich. Berlin als Kulisse verlieh dem Event zusätzlichen Charme.',
  },
  {
    src: 'https://images.ctfassets.net/b5wtya9tt2tm/2tPul6rpMzk5L1s4NoIz8L/d2bb6e58a61b27c5776bd611b12c839b/WEBSITE.jpg?fm=jpg&w=1920&q=90',
    title: 'Open Air Festival',
    description: 'Ein unglaubliches Open-Air-Festival mit den besten Künstlern der Welt. Das weitläufige Gelände und die spektakuläre Naturkulisse boten eine perfekte Atmosphäre. Vielfältige Musikstile und ein buntes Rahmenprogramm sorgten für eine unvergessliche Stimmung und Freude unter den Besuchern.',
  },
  {
    src: 'https://www.schwaebische-post.de/assets/images/34/424/34424254-wenn-die-rocknacht-einlaedt-wird-es-in-eigenzell-laut-heiss-und-voll-das-verspricht-auch-das-programm-fuer-die-60-auflage-am-mai-und-die-61-rocknacht-3pfe.jpg',
    title: 'Rock Nacht',
    description: 'Eine unvergessliche Rocknacht mit legendären Bands wie AC/DC, Metallica und The Rolling Stones. Die Energie auf der Bühne war elektrisierend, und die Menge rockte begeistert mit. Klassiker und neue Hits sorgten für eine mitreißende Atmosphäre, die die Nacht unvergesslich machte.',
  },
  {
    src: 'https://www.srf.ch/static/cms/images/960w/3a3d9e.jpg',
    title: 'Jazz Konzert - Miles Davis',
    description: 'Miles Davis unvergessliche Stücke ertönten im ganzen Stuttgarter Ballsaal! Das Jazzkonzert war ein musikalisches Highlight, bei dem die sanften Töne der Trompete und die virtuosen Improvisationen das Publikum verzauberten. Die Atmosphäre war voller Eleganz und Leidenschaft, und jeder Moment des Abends war ein Genuss für Jazzliebhaber.',
  },
  {
    src: 'https://saarklang.com/wp-content/uploads/2021/04/611B7A12-8827-440F-9421-32FDBCC41FD5.jpeg',
    title: 'Klassik Abend',
    description: 'Pierre-Olivier Queyras – Violine, Benjamin Jupé – Violoncello, und Orsolya Nagy – PianoEin klassischer Abend mit den Meisterwerken der Musikgeschichte.',
  },
  {
    src: 'https://www.falstaff-travel.com/wp-content/uploads/2022/06/190719-191633-TML2019-DN2681.jpg',
    title: 'Sommer Festival - TOMORROWLAND, BOOM/BELGIEN',
    description: 'Seit 2005 pilgern jährlich Tausende Fans elektronischer Tanzmusik ins belgische Boom, um gemeinsam in märchenhafter Kulisse zu feiern. Die Ausgabe 2019, die an zwei Wochenenden stattfand, bescherte einen Besucherrekord von 400.000 Menschen. In diesem Jahr wird an drei Wochenenden gefeiert. Headliner sind unter anderem Armin Van Buuren, Martin Garrix, Paul Kalkbrenner und Eric Prydz.',
  },
  {
    src: 'https://hf-s3.hotelfriend.com/590x417/pages/120/walpurgis-night-25301.jpg',
    title: 'Kanstatter Walpurgisnacht',
    description: 'Jedes Jahr nehmen mehr und mehr Menschen an der Walpurgisnachtsfeier teil. Die Vorbereitungen dafür starten bereits eine lange Zeit davor. In den Städten um den Brocken herum dekorieren die Menschen ihre häuser und Gärten mit Hexenfiguren. Im 21. Jahrhundert wird in vielen Ländern die Walpurgisnacht gefeiert. Doch nur in Deutschland ist dieser Tag von einer ganz besonderen Atmosphäre und Traditionen geprägt. Viele Bräuche stehen im Zusammenhang mit jungen Pärchen als Symbol für menschliche Gemeinschaft. Zwischen zwei Walpurgisfeuern durchzugehen soll eine reinigende Wirkung haben und Seuchen fernhalten, da Walpurgis eine Schutzheilige gegen die Pest, Husten und Tollwut war.',
  },
];

function Gallery() {
  return (
    <Container>
      <h2 className="text-center my-4">Galerie</h2>
      <Row className="justify-content-center mb-4">
        <Col md={6} className="mb-4">
          <Card>
            <Card.Img variant="top" src={concertImages[0].src} />
            <Card.Body>
              <Card.Title>{concertImages[0].title}</Card.Title>
              <Card.Text>{concertImages[0].description}</Card.Text>
            </Card.Body>
          </Card>
        </Col>
        <Col md={6} className="mb-4">
          <Card>
            <Card.Body>
              <Card.Title>{concertImages[1].title}</Card.Title>
              <Card.Text>{concertImages[1].description}</Card.Text>
            </Card.Body>
            <Card.Img variant="bottom" src={concertImages[1].src} />
          </Card>
        </Col>
      </Row>
      <Row className="justify-content-center mb-4">
        <Col md={4} className="mb-4">
          <Card>
            <Card.Img variant="top" src={concertImages[2].src} />
            <Card.Body>
              <Card.Title>{concertImages[2].title}</Card.Title>
              <Card.Text>{concertImages[2].description}</Card.Text>
            </Card.Body>
          </Card>
        </Col>
        <Col md={4} className="mb-4">
          <Card>
            <Card.Img variant="top" src={concertImages[3].src} />
            <Card.Body>
              <Card.Title>{concertImages[3].title}</Card.Title>
              <Card.Text>{concertImages[3].description}</Card.Text>
            </Card.Body>
          </Card>
        </Col>
        <Col md={4} className="mb-4">
          <Card>
            <Card.Img variant="top" src={concertImages[4].src} />
            <Card.Body>
              <Card.Title>{concertImages[4].title}</Card.Title>
              <Card.Text>{concertImages[4].description}</Card.Text>
            </Card.Body>
          </Card>
        </Col>
      </Row>
      <Row className="justify-content-center mb-4">
        <Col md={12}>
          <Card className="d-flex flex-row">
            <Card.Img variant="left" src={concertImages[5].src} style={{ width: '50%' }} />
            <div className="d-flex flex-column justify-content-center p-3">
              <Card.Title>{concertImages[5].title}</Card.Title>
              <Card.Text>{concertImages[5].description}</Card.Text>
            </div>
          </Card>
        </Col>
      </Row>
      <Row className="justify-content-center mb-4">
        <Col md={12}>
            <Card className="d-flex flex-row">
            <div className="d-flex flex-column justify-content-center p-3">
                <Card.Title>{concertImages[6].title}</Card.Title>
                <Card.Text>{concertImages[6].description}</Card.Text>
            </div>
            <Card.Img variant="right" src={concertImages[6].src} style={{ width: '50%' }} />
            </Card>
        </Col>
        </Row>
    </Container>
  );
}

export default Gallery;