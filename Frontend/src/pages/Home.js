import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import "../stylesheets/_all.scss";
import {Carousel, Button, Nav} from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.css';
// Components
import QuickviewCard from "../components/QuickviewCard";
import CarouselCard from "../components/CarouselCard";
import ReviewsCard from "../components/ReviewsCard";
// Images
import CardUser1 from "../images/project/Cards/user1.png";
import CardUser2 from "../images/project/Cards/user2.png";
import CardUser3 from "../images/project/Cards/user3.png";
import CardUser4 from "../images/project/Cards/user4.png";

import PaymentMethods from "../images/payment_methods.png"

import { useTranslation } from 'react-i18next';


function Home(props) {
  const [concerts, setConcerts] = useState([]);
  const [error, setError] = useState('');
  const API_URL = process.env.REACT_APP_API_URL;
  const navigate = useNavigate();

    useEffect(() => {
        fetch(`${API_URL}/api/concerts`)
            .then(response => response.json())
            .then(data => {
                console.log(data)
               const formattedData = data.map(item => ({
                    id: item.id,
                    title: item.title,
                    price: item.price,
                    available: item.available,
                    image: item.picture,
                    description: `Tickets available: ${item.available} - Price: €${item.price}`
                }));
                setConcerts(formattedData);
            })
            .catch(error => setError('Fehler beim Laden der Konzerte'));
    }, []);

  // Funktion, die die Liste in Gruppen von fünf teilt
  const chunkArray = (array, size) => {
    const chunked_arr = [];
    for (let i = 0; i < array.length; i += size) {
        chunked_arr.push(array.slice(i, i + size));
    }
    return chunked_arr;
  };

  const concertRows = chunkArray(concerts, 5);

  const handleCardClick = (concertId) => {
    // console.log("clickedCardId: " + concertId);
    navigate(`/buy?concertId=${concertId}`);
  };

  const { t } = useTranslation();
  return (
    <div>
      <div className="home">
        <div className="home-content">
          <Carousel slide className="carousel-home" style={{ width: '50rem' }} controls={false}>
            <Carousel.Item interval={5000}>
              <CarouselCard title={t('home_carousel_title_1')} description={t('home_carousel_description_1')}/>
            </Carousel.Item>
            <Carousel.Item>
            <CarouselCard title={t('home_carousel_title_2')} description={t('home_carousel_description_2')}/>
            </Carousel.Item>
            <Carousel.Item>
            <CarouselCard title={t('home_carousel_title_3')} description={t('home_carousel_description_3')}/>
            </Carousel.Item>
          </Carousel>
          <div className="button-group">
            <Button variant="warning" style={{ marginRight: '10px' }} href="#buy">{t('home_buynow_button')}</Button>
            <Button variant="light" href="#gallery">{t('home_showmore_button')}</Button>
          </div>
        </div>
      </div>
      <div className="game-card-container">
        <span id="game-text">Trending Events</span>
          {concertRows.map((row, idx) => (
              <div key={idx} className="upper-row" id="card-upper-row">
                  {row.map((concert, index) => (
                      <QuickviewCard 
                          key={index}
                          title={concert.title}
                          backgroundImage={concert.image}
                          onClick={() => handleCardClick(concert.id)}
                      />
                  ))}
              </div>
          ))}
      </div>
      <div className="reviews-container">
        <span id="game-text">Reviews</span>
        <div className="upper-row" id="reviews-upper-row">
          <ReviewsCard review="This website rocks!" pfp={CardUser1} nickname="Luis M."></ReviewsCard>
          <ReviewsCard review="Buying was a breeze!" pfp={CardUser2} nickname="Jessy K."></ReviewsCard>
          <ReviewsCard review="Great deals, seamless experience." pfp={CardUser3} nickname="Mike B."></ReviewsCard>
          <ReviewsCard review="Highly recommended, super fast! +++" pfp={CardUser4} nickname="Violetta D."></ReviewsCard>
        </div>
      </div>
      <div className="payment-section">
        <img src={PaymentMethods} alt="paymentoptions"/>
      </div>
    </div>
  );
}

export default Home;