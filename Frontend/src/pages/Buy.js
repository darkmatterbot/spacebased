import React, { useState, useEffect } from 'react';
import { Carousel, Container, Form, Button, Alert, Row, Col, Dropdown } from 'react-bootstrap';
import { useLocation } from 'react-router-dom';

function Buy() {
    const [concerts, setConcerts] = useState([]);
    const [selectedConcert, setSelectedConcert] = useState('');
    const [numberOfTickets, setNumberOfTickets] = useState(1);
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [error, setError] = useState('');
    const [successMessage, setSuccessMessage] = useState('');
    const API_URL = process.env.REACT_APP_API_URL; // http://localhost:5000
    const location = useLocation();

    useEffect(() => {
        fetchConcerts();
    }, []);

    useEffect(() => {
        const params = new URLSearchParams(location.search);
        const concertId = params.get('concertId');
        if (concertId) {
          setSelectedConcert(concertId);
        }
      }, [location]);

    const fetchConcerts = () => {
        fetch(`${API_URL}/api/concerts`)
            .then(response => response.json())
            .then(data => {
                const formattedData = data.map(item => ({
                    id: item.id,
                    title: item.title,
                    price: item.price,
                    available: item.available,
                    image: item.picture,
                    description: `Tickets available: ${item.available} - Price: €${item.price}`
                }));
                setConcerts(formattedData);
            })
            .catch(error => setError('Fehler beim Laden der Konzerte'));
    };

    const handlePurchase = () => {
        setError('');  // Reset error-string to nothing before processing the purchase

        if (!selectedConcert) {
            setError('Bitte wählen Sie zuerst ein Konzert aus.');
            return;
        }

        if (numberOfTickets < 1 || numberOfTickets > 1000) {
            setError(`Die Anzahl der Tickets muss zwischen 1 und 1000 liegen.`);
            return;
        }

        fetch(`${API_URL}/api/concerts/purchase`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                concertId: parseInt(selectedConcert),
                numberOfTickets: parseInt(numberOfTickets),
                customerInfo: { firstName, lastName, email }
            })
        })
        .then(response => {
            if (!response.ok) {
                throw new Error('Purchase failed');
            }
            return response.json();
        })
        .then(() => {
            const ticketText = numberOfTickets > 1 ? 'Tickets' : 'Ticket';
            setSuccessMessage(`${numberOfTickets} ${ticketText} erfolgreich gekauft!`);
            fetchConcerts();  // Re-fetch concerts to update the list
        })
        .catch(error => {
            console.error('Error during purchase:', error);
            setError(error.message);
        });
    };

    const selectedConcertData = concerts.find(concert => concert.id.toString() === selectedConcert);

    return (
        <Container>
            <Row className="justify-content-center mt-4">
                <Col md={8}>
                    <Carousel>
                        {concerts.map((concert, index) => (
                            <Carousel.Item key={index}>
                                <img
                                    className="d-block w-100"
                                    src={concert.image}
                                    alt={`${concert.title} poster`}
                                />
                                <Carousel.Caption>
                                    <h3>{concert.title}</h3>
                                </Carousel.Caption>
                            </Carousel.Item>
                        ))}
                    </Carousel>
                    <div className="mt-3">
                        {selectedConcertData && (
                            <div>
                                <h3>
                                    Tickets:&nbsp;
                                    <span style={{ 
                                        fontWeight: 'bold', 
                                        color: '#ffc107', 
                                        fontSize: 'larger', 
                                        marginRight: '60px'  // Breiter Abstand für bessere Trennung
                                    }}>
                                        {selectedConcertData.available}
                                    </span>
                                    Preis:&nbsp;
                                    <span style={{ 
                                        fontWeight: 'bold', 
                                        color: '#ffc107', 
                                        fontSize: 'larger'
                                    }}>
                                        €{selectedConcertData.price}
                                    </span>
                                </h3>
                            </div>
                        )}
                    </div>
                </Col>
                <Col md={4}>
                <Form>
                    <Form.Group controlId="formConcertDropdown" className="mb-3">
                        <Dropdown>
                            <Dropdown.Toggle variant="warning" id="dropdown-basic" style={{ width: '100%' }}>
                                {concerts.find(concert => concert.id.toString() === selectedConcert)?.title || 'Konzert wählen'}
                            </Dropdown.Toggle>

                            <Dropdown.Menu>
                                {concerts.map(concert => (
                                    <Dropdown.Item key={concert.id} onClick={() => setSelectedConcert(concert.id.toString())}>
                                        {concert.title}
                                    </Dropdown.Item>
                                ))}
                            </Dropdown.Menu>
                        </Dropdown>
                    </Form.Group>
                    <Form.Group controlId="formFirstName">
                        <Form.Label>Vorname</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="Vorname eingeben"
                            value={firstName}
                            onChange={e => setFirstName(e.target.value)}
                            style={{ width: '100%' }}
                        />
                        <br />
                    </Form.Group>
                    <Form.Group controlId="formLastName">
                        <Form.Label>Nachname</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="Nachname eingeben"
                            value={lastName}
                            onChange={e => setLastName(e.target.value)}
                            style={{ width: '100%' }}
                        />
                        <br />
                    </Form.Group>
                    <Form.Group controlId="formEmail">
                        <Form.Label>Email</Form.Label>
                        <Form.Control
                            type="email"
                            placeholder="Email eingeben"
                            value={email}
                            onChange={e => setEmail(e.target.value)}
                            style={{ width: '100%' }}
                        />
                        <br />
                    </Form.Group>
                    <Form.Group controlId="formTicketCount">
                        <Form.Label>Anzahl der Tickets</Form.Label>
                        <Form.Control
                            type="number"
                            value={numberOfTickets}
                            onChange={e => setNumberOfTickets(e.target.value)}
                            min="1"
                            max="1000" // Adjust maximum value as needed
                            style={{ width: '100%' }}
                        />
                        <br />
                    </Form.Group>
                    <Button variant="primary" onClick={handlePurchase}>Jetzt kaufen</Button>
                </Form>
                </Col>
            </Row>
            <br />
            <Row className="justify-content-center">
                {error && <Alert variant="danger">{error}</Alert>}
                {successMessage && (
                    <Alert variant="success">
                        <span style={{ fontWeight: 'bold', color: 'inherit', fontSize: 'inherit' }}>
                            {successMessage.split(' ')[0]}
                        </span> {successMessage.split(' ').slice(1).join(' ')}
                    </Alert>
                )}
            </Row>
        </Container>
    );
}

export default Buy;