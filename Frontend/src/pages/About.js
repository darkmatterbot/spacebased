import React from 'react';
import { Container, Row, Col, Card, Image, Button, Alert } from 'react-bootstrap';
// Images
import Oliver from "../images/project/About/oliver.png";
import Julian from "../images/project/About/julian.png";
import Jonathan from "../images/project/About/jonathan.png";

function About() {
    return (
        <Container className="mt-5">
            {/* Embedded Astronaut GIF */}
            <div style={{ position: 'absolute', top: 60, right: 40, width: '200px', height: '200px', zIndex: 1}}>
                <iframe src="https://giphy.com/embed/3OXc5iM4VybLzKAoBR" width="100%" height="100%" style={{ border: 'none' }} frameBorder="0" class="giphy-embed" allowFullScreen></iframe>
                <div style={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, cursor: 'default' }}></div>
            </div>

            {/* Header Alert Section */}
            <Alert variant="info" className="text-center" >
                Willkommen auf unserem Ticketsystem! Erfahren Sie mehr über unser Team und unsere Mission.
            </Alert>

            {/* Header Section */}
            <Row className="justify-content-center mb-5">
                <Col md={10} className="text-center">
                    <h1 className="display-4">Über Uns</h1>
                    <p className="lead">
                        Wir sind drei Masterstudenten aus dem Bereich Software-Architektur: Oliver Hahn, Julian Lingnau und Jonathan Maus. Unsere Mission ist es, eine innovative und skalierbare Ticketing-Plattform unter Verwendung von Space-Based-Architektur-Prinzipien zu schaffen.
                    </p>
                </Col>
            </Row>

            {/* Team Members Section */}
            <Row className="justify-content-center mb-5">
                <Col md={4} className="text-center">
                    <Card className="shadow-sm h-100">
                        <Card.Body className="d-flex flex-column align-items-center">
                            <Image 
                                src={Oliver}
                                roundedCircle
                                className="mb-3"
                                style={{ width: '150px', height: '150px', objectFit: 'cover' }}
                            />
                            <Card.Title>Oliver Hahn</Card.Title>
                            <Card.Text className="flex-grow-1 text-center">
                                Oliver ist ein Software-Architekt mit einer Leidenschaft für die Erstellung skalierbarer und effizienter Systeme. Seine Expertise in verteilten Systemen und Space-Based-Architektur bildet das Rückgrat unserer Plattform.
                            </Card.Text>
                            <Button variant="outline-primary" href="mailto:olihahn19@gmail.com">Kontaktieren</Button>
                        </Card.Body>
                    </Card>
                </Col>

                <Col md={4} className="text-center">
                    <Card className="shadow-sm h-100">
                        <Card.Body className="d-flex flex-column align-items-center">
                            <Image 
                                src={Julian}
                                roundedCircle
                                className="mb-3"
                                style={{ width: '150px', height: '150px', objectFit: 'cover' }}
                            />
                            <Card.Title>Julian Lingnau</Card.Title>
                            <Card.Text className="flex-grow-1 text-center">
                                Julian ist ein Full-Stack-Entwickler mit einem Talent für benutzerfreundliche Oberflächen. Seine Fähigkeiten in der Front-End- und Back-End-Entwicklung sorgen dafür, dass unsere Plattform sowohl funktional als auch ansprechend ist.
                            </Card.Text>
                            <Button variant="outline-primary" href="mailto:lingnau.julian@web.de">Kontaktieren</Button>
                        </Card.Body>
                    </Card>
                </Col>

                <Col md={4} className="text-center">
                    <Card className="shadow-sm h-100">
                        <Card.Body className="d-flex flex-column align-items-center">
                            <Image 
                                src={Jonathan}
                                roundedCircle
                                className="mb-3"
                                style={{ width: '150px', height: '150px', objectFit: 'cover' }}
                            />
                            <Card.Title>Jonathan Maus</Card.Title>
                            <Card.Text className="flex-grow-1 text-center">
                                Jonathan ist ein DevOps-Spezialist, der dafür sorgt, dass unsere Plattform reibungslos und effizient läuft. Seine Expertise in Cloud-Infrastruktur und kontinuierliches Monitoring ist entscheidend für die Aufrechterhaltung unserer hohen Verfügbarkeit und Leistung.
                            </Card.Text>
                            <Button variant="outline-primary" href="mailto:jomaus@protonmail.com">Kontaktieren</Button>
                        </Card.Body>
                    </Card>
                </Col>
                
            </Row>

            {/* Vision Section */}
            <Alert variant="primary" className="text-center">
                <Alert.Heading>Unsere Vision</Alert.Heading>
                <p>
                    Unsere Vision ist es, die Ticketing-Industrie zu revolutionieren, indem wir fortschrittliche Architekturprinzipien nutzen, um eine Plattform zu schaffen, die jede Last bewältigen kann. Wir glauben an die Kraft der Technologie, Menschen zusammenzubringen und Eventerlebnisse für alle zugänglicher zu machen.
                </p>
                <hr />
                <p className="mb-0">
                    Vielen Dank!
                </p>
            </Alert>
        </Container>
    );
}

export default About;