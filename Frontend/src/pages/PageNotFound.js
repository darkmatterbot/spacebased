import React from "react";
import {Container, Nav} from "react-bootstrap";
require("../stylesheets/_all.scss");

// PageNotFound 404 Error -> Wenn falsche Route eingegeben wurde oder User sich vertippt hat,
// Wird man immer zu dieser Seite weitergeleitet, mit der man wieder zu Home kommt.
export default class PageNotFound extends React.Component {

  render() {
    return (
      <div className="pagenotfound">
        <Container>
          <h1 style={{fontSize: '18vh'}} >404_<br /></h1>
          <h1 style={{fontSize: '3vh', paddingBottom: '4vh'}}>SHOW'S OVER HERE – FIND YOUR WAY BACK.</h1>
          <button type="button" className="btn--outline--large" onClick={()=> window.scrollTo( 0, 0 )}><Nav.Link href="#home" style={{color: 'white'}}>BACK HOME</Nav.Link></button>
        </Container>
      </div>
    );
  }
}