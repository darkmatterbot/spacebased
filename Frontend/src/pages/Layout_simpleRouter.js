import React from "react"
import { HashRouter, Route, Routes } from "react-router-dom"; // HashRouter importieren
import NavigationBar from "../components/NavigationBar";
import Footer from "../components/Footer";
import Home from "./Home";
import Buy from "./Buy"
import Gallery from "./Gallery";
import About from "./About"
import PageNotFound from "./PageNotFound";
require("../stylesheets/_all.scss");

// setzt für bestimmte URLs die Pages fest, die dann aufgerufen werden sollen
// Switch sorgt dafür, dass wenn ein Fall zutrifft, dieser auch genommen wird, wenn am Ende angelangt ohne Treffer
// PrivateRoute gekennzeichnete Routen können nicht ohne bereits eingeloggt sein betreten werden
// wird man zu PageNotFound aufgerufen. -> Somit wird man egal was man eingibt immer richtig weitergeleitet

// Bei Route /home will ich props übergeben deshalb muss man es über eine Funktion übergeben siehe Example hier: https://learnwithparam.com/blog/how-to-pass-props-in-react-router/
function Layout() {
    return (
        <HashRouter>
            <div style={{ display: 'flex', flexDirection: 'column', minHeight: '100vh' }}>
                <NavigationBar />
                <div style={{ flex: 1 }}>
                    <Routes>
                        <Route path="/" element={<Home/>} />
                        <Route path="/home" element={<Home/>} />
                        <Route path="/buy" element={<Buy/>} />
                        <Route path="/gallery" element={<Gallery/>} />
                        <Route path="/about" element={<About/>} />
                        <Route path="*" element={<PageNotFound/>} />
                    </Routes>
                </div>
                <Footer/>
            </div>
        </HashRouter>
    );
}

export default (Layout);