import Card from 'react-bootstrap/Card';

function CarouselCard(props) {
    return (
        <div className="d-flex justify-content-center">
        <Card style={{ width: '40rem', border: 'none', backgroundColor: 'transparent', color: 'white', height: '250px'}}>
          <Card.Body className="text-center">
            <Card.Title style={{ fontSize: '50px' }}className="card-title">{props.title}</Card.Title>
            <Card.Text>
                {props.description}
            </Card.Text>
          </Card.Body>
        </Card>
      </div>
    );
  }
  
  export default CarouselCard;