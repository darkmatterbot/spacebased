import React from "react";
import { Card } from "react-bootstrap";
import "../stylesheets/_gameCard.css";

function QuickviewCard(props) {
  return (
    <div className="game-card" style={{backgroundImage: `url(${props.backgroundImage})`}} onClick={props.onClick}>
      <Card.Body>
        <Card.Link href="#">{props.title}</Card.Link>
      </Card.Body>
    </div>
  );
}

export default QuickviewCard;
