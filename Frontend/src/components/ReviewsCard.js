import React from "react";
import { Card, CardText } from "react-bootstrap";
import "../stylesheets/_reviewsCard.css";

function ReviewsCard(props) {
  return (
    <div className="reviews-card">
        <CardText className="review-text">{props.review}</CardText>
        <div className="review-owner">
          <Card.Img variant="left" src={props.pfp} className="pfp rounded-circle" />
          <Card.Text className="nickname-text">{props.nickname}</Card.Text>
        </div>
    </div>
  );
}

export default ReviewsCard;
