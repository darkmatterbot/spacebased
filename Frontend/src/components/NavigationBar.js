import React, { useEffect, useState } from "react";
import "../stylesheets/_navigationBar.css";
import { Navbar, Dropdown, Nav, Button, Container, Image } from "react-bootstrap";
import Logo from "../images/project/logo.png";
import { useTranslation } from 'react-i18next';
import 'flag-icons/css/flag-icons.min.css';
import 'holderjs'

const flags = {
  USD: 'us',
  EUR: 'eu',
  GBP: 'gb',
  AUD: 'au',
  CAD: 'ca',
  RUB: 'ru',
  SGD: 'sg',
  TWD: 'tw',
  HKD: 'hk',
  CNY: 'cn',
  JPY: 'jp',
  BRL: 'br',
  MXN: 'mx',
  en: 'gb',  // Englisch wird durch die Flagge von Großbritannien dargestellt
  de: 'de',  // Deutschland Flagge
  tr: 'tr'   // Türkische Flagge
};

function NavigationBar(props) {

  const { i18n } = useTranslation();
  const [currentLanguage, setCurrentLanguage] = useState(i18n.language);
  
  useEffect(() => {
    const savedLanguage = localStorage.getItem('i18nextLng');
    if (savedLanguage) {
      i18n.changeLanguage(savedLanguage);
      setCurrentLanguage(savedLanguage);
    } else {
      setCurrentLanguage(i18n.language);
    }
  }, [i18n]);

  const changeLanguage = (lng) => {
    i18n.changeLanguage(lng);
    localStorage.setItem('i18nextLng', lng);
    setCurrentLanguage(lng);
  };

  const languageOptions = {
    en: 'English',
    de: 'Deutsch',
    tr: 'Türkçe'
  };

  return (
    <Navbar expand="lg" bg="dark" variant="dark" className="px-3">
          <Navbar.Brand href="#home">
            <img
              src={Logo}
              className="logoImageNav"
              style={{marginLeft: '15px'}}
              alt="Logo"
            />
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
              <Nav.Link href="#home">Home</Nav.Link>
              <Nav.Link href="#buy">Buy Tickets</Nav.Link>
              <Nav.Link href="#gallery">Gallery</Nav.Link>
              <Nav.Link href="#about">About</Nav.Link>
            </Nav>
            <Nav className="justify-content-end" style={{ marginRight: '0px' }}>
              <Dropdown data-bs-theme="dark" className="me-3">
                <Dropdown.Toggle id="language-dropdown" variant="outline-light" style={{ display: 'flex', alignItems: 'center' }}>
                  <span className={`fi fi-${flags[currentLanguage]} rounded-circle`} style={{ width: '20px', height: '20px', marginRight: '8px', backgroundSize: 'cover' }}></span>
                  {languageOptions[currentLanguage]}
                </Dropdown.Toggle>
                <Dropdown.Menu>
                  {Object.keys(languageOptions).map(lng => (
                      <Dropdown.Item key={lng} onClick={() => changeLanguage(lng)} style={{ display: 'flex', alignItems: 'center', lineHeight: '20px' }}>
                          <span className={`fi fi-${flags[lng]} rounded-circle`} style={{ width: '20px', height: '20px', backgroundSize: 'cover' }}></span>
                          {languageOptions[lng]}
                      </Dropdown.Item>
                  ))}
                </Dropdown.Menu>
              </Dropdown>
              <Nav.Item className="d-flex align-items-center" style={{ color: 'white' }}>
                Julia
                <Image
                  roundedCircle
                  src="holder.js/20x20?text=J&bg=#FFD700&fg=FFF"
                  style={{ marginLeft: '5px' }}
                />
              </Nav.Item>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
  );
}

export default NavigationBar;