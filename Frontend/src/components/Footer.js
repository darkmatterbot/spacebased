import React from "react";
import { Nav, NavItem } from "react-bootstrap";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faInstagram, faTwitter, faFacebook } from '@fortawesome/free-brands-svg-icons';
// Images
import Logo from "../images/project/logo.png";
import "../stylesheets/_all.scss";

export default class Footer extends React.Component {

  render() {
    const logoImage = {
      width: 30,
      marginRight: '0.1vh',
    };

    return (
      <div className="footer-container">
        <section className="footer-section">
          <div className="footer-section-wrap" onClick={()=> window.scrollTo( 0, 0 )}>
            <NavItem>
              <div className="logoBottom">
                <img
                  alt="Logo"
                  src={Logo}
                  style={logoImage}
                />
                Space-Based
              </div>
            </NavItem>
            <NavItem className="logoBottom">
              Your gateway to unforgettable live music experiences.
            </NavItem>
            <NavItem href="https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwiVhoC98uX_AhV9W_EDHZSdBWAQFnoECA4QAQ&url=https%3A%2F%2Fde.linkedin.com%2Fin%2Fjulian-lingnau-05b623162&usg=AOvVaw1YSl6_yJZVx5llEp2MZ5dj&cshid=1687953018164118&opi=89978449" target="_blank" rel="noopener noreferrer" className="logoBottom">
              Made with ❤️ by <br/> Oliver, Jonathan & Julian <br/> {(new Date().getUTCFullYear())}
            </NavItem>
            <Nav className="socialMediaIconStyle">
            <NavItem href="https://www.instagram.com/yourinstagram">
              <FontAwesomeIcon icon={faInstagram} size="lg" />
            </NavItem>
            <NavItem href="https://twitter.com/yourtwitter">
              <FontAwesomeIcon icon={faTwitter} size="lg" />
            </NavItem>
            <NavItem href="https://www.facebook.com/yourfacebook">
              <FontAwesomeIcon icon={faFacebook} size="lg" />
            </NavItem>
          </Nav>
          </div>
        </section>
      </div>
    );
  }
}