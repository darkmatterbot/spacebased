import React from 'react';
import ReactDOM from 'react-dom';
import './stylesheets/index.css';
import "bootstrap/dist/css/bootstrap.min.css";
import './i18n'; // Importieren der i18next-Konfiguration für Sprachen
import Layout from './pages/Layout_simpleRouter';

ReactDOM.render(
  <React.StrictMode>
    <Layout />
  </React.StrictMode>,
  document.getElementById('root')
);