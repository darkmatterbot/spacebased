# SpaceBased



## Getting started
In order to run the project simply run in the root directory following command:
```bash
docker compose up
```
This will launch all containers for backend and frontend if no output on the terminal is needed
add the `-d` option.

## Overview
| port | application          | notes               |
|------|----------------------|---------------------|
| 80   | frontend             | http://localhost    |
| 8080 | hazelcast management | hazelcast dashboard |
| 8090 | adminer              | mysql dashboard     |

### logins
- DB_USER = 'myuser'
- DB_PASS = 'mypassword'
- DB_DB = 'mydatabase'

